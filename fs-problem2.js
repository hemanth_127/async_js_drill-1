const fs = require('fs')
const path = require('path')
const relativePath = '../AsyncJs'

function readFile (callback) {
  const filePath = 'lipsum.txt'
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error(`file error`, err)
    } else {
      console.log(`sucessfully read the file` + `\n${filePath}\n\n${data}\n`)
      callback(data, filePath)
    }
  })
}

function FileToUppercase (data, filePath, callback) {
  const upperdata = data.toUpperCase()
  const uppercaseFileName = 'uppercase.txt'

  fs.writeFile(uppercaseFileName, upperdata, 'utf8', err => {
    if (err) {
      console.error(`while writing `(err))
    } else {
      console.log(`successfully converted to uppercase \n\n${upperdata}\n`)
      callback(uppercaseFileName, filePath)
    }
  })
}

// 3: Read the new file convert to lowercase & split into sentences and write to a new file
function writeLowercaseSentencesToFile (data, filePath, callback) {
  if (data) {
    const lowercaseData = data.toLowerCase()
    const sentences = lowercaseData.split(/[.!?]/).filter(Boolean)
    const lowercaseFileName = 'lowercase_sentences.txt'

    fs.writeFile(lowercaseFileName, sentences.join('\n'), 'utf8', err => {
      if (err) {
        console.error(`while writing`(err))
      } else {
        console.log(`successfully lowercased \n\n${lowercaseData}\n`)
        callback(lowercaseFileName, filePath)
      }
    })
  }
}

// 4: Read the new files, sort the content and write the new file names to a new file
function sortAndWriteToFile (files, filePath, callback) {
  const sortedData = files.sort().join('\n')
  const sortedFile = 'filenames.txt'

  fs.writeFile(sortedFile, sortedData, 'utf8', err => {
    if (err) {
      console.error(`while writing`(err))
    } else {
      console.log(`data successfully sorted \n\n${sortedData}\n`)
      callback(sortedFile, filePath)
    }
  })
}

// 5: Read filenames.txt and delete all new files mentioned in the list
function deleteFiles (fileList, filePath, callback) {
  fileList.forEach(fileName => {
    if (fs.existsSync(fileName)) {
      fs.unlink(fileName, err => {
        if (err) {
          console.error(`writing `, err)
        } else {
          console.log(`successfully deleted \n${fileName}`)
          callback(filePath)
        }
      })
    } else {
      console.log(`not found the file`)
      callback(filePath)
    }
  })
}

function problem2 (relativePath) {
  readFile((data, filePath) => {
    if (data) {
      FileToUppercase(data, filePath, (uppercaseFileName, filePath) => {
        writeLowercaseSentencesToFile(
          data,
          filePath,
          (lowercaseFileName, filePath) => {
            sortAndWriteToFile(
              [uppercaseFileName, lowercaseFileName],
              filePath,
              () => {
                deleteFiles(
                  [uppercaseFileName, lowercaseFileName],
                  filePath,
                  () => {
                    console.log('successfully completed.\n')
                  }
                )
              }
            )
          }
        )
      })
    } else {
      console.error(`reading error `)
    }
  })
}

module.exports = problem2
