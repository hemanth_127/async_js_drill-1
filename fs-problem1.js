const fs = require('fs')
const path = require('path')

function createRandomJsonFiles (directoryPath, numberOfFiles, callback) {
  fs.mkdir(directoryPath, { recursive: true }, err => {
    if (err) {
      callback(err)
      return
    }

    let filesCreated = 0

    for (let index = 0; index < numberOfFiles; index++) {
      const fileName = `file_${index}.json`
      const filePath = path.join(directoryPath, fileName)
      const randomData = { value: Math.random() }

      fs.writeFile(filePath, JSON.stringify(randomData), err => {
        if (err) {
          callback(err)
          return
        }

        filesCreated++
        if (filesCreated === numberOfFiles) {
          callback(null)
        }
      })
    }
  })
}

function deleteFilesInDirectory (directoryPath, callback) {
  fs.readdir(directoryPath, (err, files) => {
    if (err) {
      callback(err)
      return
    }

    let filesDeleted = 0

    if (files.length === 0) {
      callback(null)
      return
    }

    files.forEach(file => {
      const filePath = path.join(directoryPath, file)
      fs.unlink(filePath, err => {
        if (err) {
          callback(err)
          return
        }

        filesDeleted++
        if (filesDeleted === files.length) {
          callback(null)
        }
      })
    })
  })
}

function fsProblem1 (directoryPath, numberOfFiles, callback) {
  createRandomJsonFiles(directoryPath, numberOfFiles, err => {
    if (err) {
      console.error('Error creating random JSON files:', err)
      return
    }

    console.log('Random JSON files created successfully.')

    deleteFilesInDirectory(directoryPath, err => {
      if (err) {
        console.error('Error deleting files in directory:', err)
        return
      }
      console.log('Files in directory deleted successfully.')
    })
  })
}

module.exports = fsProblem1
