const { log } = require('console')
const fsProblem1 = require('./../fs-problem1')

const absolutePathOfRandomDirectory = './random_files'

const randomNumberOfFiles = 5

try {
  fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles)
} catch (err) {
  console.log(err, 'error caught in fs-problem 1')
}
