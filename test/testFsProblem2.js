const path = './AsyncJs'

const problem2 = require('../fs-problem2')

try {
  problem2(path)
} catch (err) {
  console.log(err, 'error caught in fs-problem 2')
}
